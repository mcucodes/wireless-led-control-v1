/* --COPYRIGHT--,BSD_EX
 * Copyright (c) 2012, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *******************************************************************************
 * 
 *                       MSP430 CODE EXAMPLE DISCLAIMER
 *
 * MSP430 code examples are self-contained low-level programs that typically
 * demonstrate a single peripheral function or device feature in a highly
 * concise manner. For this the code may rely on the device's power-on default
 * register values and settings such as the clock configuration and care must
 * be taken when combining code from several examples to avoid potential side
 * effects. Also see www.ti.com/grace for a GUI- and www.ti.com/msp430ware
 * for an API functional library-approach to peripheral configuration.
 *
 * --/COPYRIGHT--*/
//******************************************************************************
//   MSP430G2xx3 Demo - USCI_A0, UART 9600 Full-Duplex Transceiver, 32kHz ACLK
//
//   Description: USCI_A0 communicates continously as fast as possible full-
//   duplex with another device. Normal mode is LPM3, with activity only during
//   RX and TX ISR's. The TX ISR indicates the USCI_A0 is ready to send another
//   character. The RX ISR indicates the USCI_A0 has received a character. At
//   9600 baud, a full character is tranceived ~1ms.
//   The levels on P1.3/P1.4 are TX'ed. RX'ed value is displayed on P1.6/0.  
//   ACLK = BRCLK = LFXT1 = 32768Hz, MCLK = SMCLK = DCO ~1.2MHz
//   Baud rate divider with 32768Hz XTAL @9600 = 32768Hz/9600 = 3.41
//* An external watch crystal is required on XIN XOUT for ACLK *//
//
//                 MSP430G2xx3                  MSP430G2xx3
//              -----------------            -----------------
//             |              XIN|-      /|\|              XIN|-
//             |                 | 32kHz  | |                 | 32kHz
//             |             XOUT|-       --|RST          XOUT|-
//             |                 | /|\      |                 |
//             |              RST|---       |                 |
//             |                 |          |                 |
//           ->|P1.3             |          |             P1.0|-> LED
//           ->|P1.4             |          |             P1.6|-> LED
//       LED <-|P1.0             |          |             P1.3|<-
//       LED <-|P1.6             |          |             P1.4|<-
//             |     UCA0TXD/P1.2|--------->|P1.1             |
//             |                 |   9600   |                 |
//             |     UCA0RXD/P1.1|<---------|P1.2             |
//
//
//   D. Dang
//   Texas Instruments Inc.
//   February 2011
//   Built with CCS Version 4.2.0 and IAR Embedded Workbench Version: 5.10
//******************************************************************************
#include <msp430.h>



#define GLED_ON() P1OUT |= 0x40;
#define GLED_OFF() P1OUT &= ~0x40;
#define GLED_TOG() P1OUT ^= 0x40;

#define RLED_ON() P1OUT |= 0x01;
#define RLED_OFF() P1OUT &= ~0x01;
#define PLED_ON() P1OUT |= 0x04;

volatile int connected=0;
volatile int led_pwm=0;


void send_char(char);
void send_string(char*);
void delay(char);
char rxData[8];
unsigned int tic_count=0;


int main(void)
{
  WDTCTL = WDTPW + WDTHOLD;                 // Stop watchdog timer
  if (CALBC1_1MHZ==0xFF)					// If calibration constant erased
  {
    while(1);                               // do not load, trap CPU!!
  }
  DCOCTL = 0;                               // Select lowest DCOx and MODx settings
  BCSCTL1 = CALBC1_1MHZ;                    // Set DCO
  DCOCTL = CALDCO_1MHZ;
  UCA0CTL1 |= UCSSEL_2;

  P1OUT = 0x00;                             // P1.0/6 setup for LED output
  P1DIR = BIT2 + BIT6;
  P1SEL = BIT1 + BIT2;                     // P1.1 = RXD, P1.2=TXD, P1.0 PWM out
  P1SEL2 = BIT1;

  UCA0BR0 = 104;                            // 8MHz 9600
  UCA0BR1 = 0;

  UCA0MCTL = UCBRS1 + UCBRS0;               // Modulation UCBRSx = 3
  UCA0CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**

  CCR0 = 512-1;                             // PWM Period
  CCTL1 = OUTMOD_7;                         // CCR1 reset/set
  CCR1 = 0;                            // CCR1 PWM duty cycle


  IE2 |= UCA0RXIE;               // Enable USCI_A0 TX/RX interrupt
  TACTL = TASSEL_2 + MC_1 + TAIE;           // SMCLK, contmode, interrupt

  __bis_SR_register( GIE);       // Enter LPM3 w/ interrupts enabled

while(1);
}

void send_char(char txbyte)
{

	while (!(IFG2&UCA0TXIFG));
	UCA0TXBUF=txbyte;
}



void send_string(char *string)
{

	while(*string!='\0'){
	send_char(*string++);
	}
}

void delay(char dly)
{
while(dly--){
	_delay_cycles(9000);
}

}



// USCI A0/B0 Transmit ISR
#pragma vector=USCIAB0TX_VECTOR
__interrupt void USCI0TX_ISR(void)
{
}

// USCI A0/B0 Receive ISR
#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
	static unsigned int count=0;

  rxData[count++] = UCA0RXBUF;                        // Display RX'ed charater
  if(count==8){

	  if(rxData[7]=='1'){
			  if(rxData[6]=='1'){
				led_pwm =(100)*(rxData[1]-48)+(10)*(rxData[2]-48)+(rxData[3]-48);
				CCR1=2*led_pwm;
			  }
			  else
				  CCR1=510;


	  }
	  else
		  CCR1=0;

	  tic_count++;

	  count=0;
  }

}


// Timer_A3 Interrupt Vector (TA0IV) handler
#pragma vector=TIMER0_A1_VECTOR
__interrupt void Timer_A(void)
{
 switch( TA0IV )
 {
   case  2: break;                          // CCR1 not used
   case  4: break;                          // CCR2 not used
   case  10:
	   if(tic_count> 20 && tic_count<=22){
		   GLED_ON();
		   if(tic_count==22)
			   tic_count=0;
	   }
	   else
		   GLED_OFF();
	   break;								// overflow

	   }


}



